<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Contact Us</title>
  </head>
  <body>

    <form action="/contact" method="post">
      {{ csrf_field() }}
      Email: <input type="text" name="email">
      <br>
      <textarea name="message" rows="8" cols="80"></textarea>
      <br>
      <button type="submit">Send</button>
    </form>

  </body>
</html>
