<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Search</title>
  </head>
  <body>
    <h1>Search</h1>
    <form action="/search" method="get">
      Artist: <input type="text" name="artist">
      <button type="submit">Search</button>
    </form>
  </body>
</html>
