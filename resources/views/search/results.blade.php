<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Search Results</title>
  </head>
  <body>

    <p>
      You search for: {{ $artist }}
    </p>

    <ul>
      @foreach ($songs as $song)
        <li>
          {{ $song->title }} by {{ $song->artist_name }}
        </li>
      @endforeach
    </ul>

  </body>
</html>
