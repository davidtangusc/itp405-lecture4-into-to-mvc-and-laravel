<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    public function index()
    {
        return view('search.index');
    }

    public function search()
    {
        $artist = request('artist');

        if (!$artist) {
            return redirect('/');
        }

        $songs = DB::table('songs')
            ->select('title', 'artist_name')
            ->join('artists', 'songs.artist_id', '=', 'artists.id')
            ->where('artist_name', 'like', "%$artist%")
            ->get();

        // dd($songs);

        return view('search.results', [
            'artist' => $artist,
            'songs' => $songs
        ]);
    }
}
