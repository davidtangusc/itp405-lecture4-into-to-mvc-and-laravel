<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.form');
    }

    public function submit()
    {
        DB::table('messages')->insert([
            'email' => request('email'),
            'message' => request('message')
        ]);
        
        return redirect('/contact');
    }
}
